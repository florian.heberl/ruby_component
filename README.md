# Ruby

The Ruby CI components provide a way to [lint](#lint) and [test](#test) Ruby code.

## All Jobs

The `all-jobs` component includes both the [lint](#lint) and [test](#test) components.

You can add it to your `.gitlab-ci.yml` by adding a block along the lines of:

```yml
include:
  - component: gitlab.com/components/ruby/all-jobs@~latest
    inputs:
      project_path: example_project
      ruby_image: ruby:3.2
```

## Lint

The `lint` component runs the static code analyzer [Rubocop](https://github.com/rubocop/rubocop) against your Ruby code. 

![Rubocop summary](./images/lint.png)

You can add it to your `.gitlab-ci.yml` by adding a block along the lines of:

```yml
include:
  - component: gitlab.com/components/ruby/lint@~latest
    inputs:
      job_name: lint
      project_path: example_project
      ruby_image: ruby:3.2
      stage: test
```

## Test

The `test` component runs [RSpec](https://rspec.info/) tests for your Ruby code.
Providing:

A summary in merge requests (indicating code coverage, and number of failed and success tests):

![Merge request summary](./images/mr-summary.png)

A detailed breakdown of tests (including duration and success rate):

![Test result summary](./images/test.png)

And, code coverage visualization alongside the diff:

![Code coverage visualization](./images/coverage.png)

You can add it to your `.gitlab-ci.yml` by adding a block along the lines of:

```yml
include:
  - component: gitlab.com/components/ruby/test@~latest
    inputs:
      coverage_report_path: coverage/coverage.xml
      job_name: test
      junit_report_path: rspec.xml
      project_path: example_project
      ruby_image: ruby:3.2
      stage: test
```
