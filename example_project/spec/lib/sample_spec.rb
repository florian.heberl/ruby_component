# frozen_string_literal: true

require_relative '../../lib/sample'

RSpec.describe Sample do
  it 'works' do
    expect { described_class.sample }.not_to raise_error
  end
end
