# frozen_string_literal: true

# This is a sample class
class Sample
  def self.sample
    puts 'Hello world!'
  end
end
